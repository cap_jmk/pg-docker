# Fast pg deploy 

Aim is to have data that can be backed up. 

Read 
[Medium article](https://medium.com/codex/how-to-persist-and-backup-data-of-a-postgresql-docker-container-9fe269ff4334)

Clone the git 

```
https://codeberg.org/cap_jmk/pg-docker.git
```

Install Docker with the official convenience script

```
curl -fsSL https://get.docker.com -o get-docker.sh
DRY_RUN=1 sh ./get-docker.sh
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

Simply run

```
docker compose up -d
```